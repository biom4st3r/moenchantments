plugins {
    id 'java'
    id 'fabric-loom' version '0.10-SNAPSHOT'
	id 'io.github.juuxel.loom-quiltflower' version '1.8.0'
}

def getVersion(project) {
	switch(project.name) {
		case 'moenchantments':
			return rootProject.rootVersion
		case 'particle_emitter':
			return rootProject.partcile_emitter_version
		case 'more-enchantments':
			return rootProject.more_enchantment_verison
		case 'moenchant_lib':
			return rootProject.moenchant_lib_version
		case 'expression_lib':
			return rootProject.expression_lib_version
		case 'client_enchantments':
			return rootProject.clientenchantments_version
		default:
			System.out.println(project.name)
			return 'ERROR'
	}
}

allprojects {
    apply plugin: 'java'
    // apply plugin: 'java-library'
    apply plugin: 'fabric-loom'

    group = 'biom4st3r.mods'
    version = getVersion(it)
    archivesBaseName = project.archives_base_name

    sourceCompatibility = targetCompatibility = JavaVersion.VERSION_17

    // Declare dependencies
    dependencies {
        // Fabric
        minecraft "com.mojang:minecraft:${project.minecraft_version}"
        mappings "net.fabricmc:yarn:${project.minecraft_version}+${project.yarn_mappings}"
        modImplementation "net.fabricmc:fabric-loader:${project.loader_version}"
		
		modImplementation "biom4st3r.libs:reflection:0.1.1"
		include "biom4st3r.libs:reflection:0.1.1"
        // Mods
        modImplementation "net.fabricmc.fabric-api:fabric-api:${project.fabric_version}"
        modImplementation "com.terraformersmc:modmenu:2.0.2"

        // Subprojects
        subprojects.each {
            implementation project(path: ":${it.name}", configuration: 'namedElements')
            // include project("${it.name}:") // nest within distribution
        }
    }

	// loom {
	// 	shareCaches = true
	// }

    // Produce a sources distribution
    java {
        withSourcesJar()
    }

    // Add the licence to all distributions
    tasks.withType(Jar).configureEach {
        it.from rootProject.file('LICENCE')
    }

    // Process any resources
	processResources {
        inputs.property 'version', project.version

		filesMatching("fabric.mod.json") {
			expand "version": project.version
		}
    }

    // tasks.withType(JavaCompile).configureEach {
	// 	// ensure that the encoding is set to UTF-8, no matter what the system default is
	// 	// this fixes some edge cases with special characters not displaying correctly
	// 	// see http://yodaconditions.net/blog/fix-for-java-file-encoding-problems-with-gradle.html
	// 	// If Javadoc is generated, this must be specified in that task too.
	// 	it.options.encoding = "UTF-8"

	// 	// Minecraft 1.17 (21w19a) upwards uses Java 16.
	// 	it.options.release = 17
	// }

    // Add any additional repositories
    repositories {
        mavenCentral()
        maven { name 'Fabric'; url 'https://maven.fabricmc.net/' }
        maven { name 'TerraformersMC'; url 'https://maven.terraformersmc.com/' }
		maven {
			name 'Biow0rks'
			url "https://gitlab.com/api/v4/projects/29859041/packages/maven"
		}
    }
}

subprojects {
    apply plugin: 'maven-publish'
	
	// Define how packages are published
	publishing {
		// Declare all publications
		publications {
			mavenJava(MavenPublication) { from components.java }
			// mavenJava(MavenPublication) {
			// 	// Main
			// 	artifact(remapJar) { builtBy remapJar }
			// 	// Sources
			// 	artifact(sourcesJar) { builtBy remapSourcesJar }
			// }
		}

		// Add repositories to publish to
		repositories {
			// GitHub Packages (https://pkg.github.com)
			maven {
				name 'GitLab'
				url "https://gitlab.com/api/v4/projects/29859041/packages/maven"
				artifactUrls "https://gitlab.com/api/v4/projects/29859041/packages"
				credentials(HttpHeaderCredentials) {
					name = "Private-Token"
					value = System.getenv('GITLAB_TOKEN')
				}
				authentication {
					header(HttpHeaderAuthentication)
				}
			}
		}
	}
	it.tasks.publish.dependsOn it.tasks.build
	it.tasks.publishToMavenLocal.dependsOn it.tasks.build
}
