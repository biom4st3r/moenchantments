import re, os, sys, difflib

regex = [
    # Incorrect Spacing on commas (arg0,arg1,arg2) -> (arg0, arg1, arg2)
    (r'(\w),(\w)' ,                       r'\1, \2'),
    # replace C style brackets with java
    (r'\)[ ]{0,}\n[ ]{0,}\{' ,            r') {'),
    # removed brackets from single arg lambda
    (r'\(([a-zA-Z0-9]{1,})\)(\W{0,})->' , r'\1\2->'),
    # Replace (notspace)-> with (notspace) ->
    (r'([^\s])->' ,                       r'\1 ->'),
    # Replace 1line lambda bodies (asdf)->{return true;}
    (r'->([ ]{0,})\{return (.*);\}' ,     r'->\1\2'),
    # for|if followed by non-space
    (r'( )(for|if)(\()' ,                 r' \2 \3'),
    # Replace ->(not whitespace)
    (r'->([^ ])' ,                        r'-> \1')
]

if len(sys.argv) < 2:
    print("needs path as arg")
    sys.exit()
args = sys.argv[1]
# print(args)

def doRegex(text : str) -> str:
    for pair in regex:
        newText = re.sub(pair[0], pair[1], text) # enact
        while newText != text:
            text = newText # save result
            newText = re.sub(pair[0], pair[1], text) # re-enact
        text = newText
    return text

def main():
    for path, y, files in os.walk(args):
        for file in files:
            if not file.endswith('.java'): continue
            with open(path + '/' + file,'rb+') as openFile:
                
                contents = openFile.read().decode('utf-8')
                output = list(difflib.unified_diff( [contents] , [doRegex(contents)]))
                if len(output) == 0: continue
                
                # print(contents[0])
                # print(doRegex(contents[0])[0])
                print(file)
                print(''.join(output))
                input()

main()