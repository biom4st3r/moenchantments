package com.biom4st3r.moenchantments.mixin.moenchant_lib.events;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.events.OnCrossBowArrowCreationEvent;
import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(CrossbowItem.class)
public abstract class OnCrossBowArrowCreationEventMxn {
    @Inject(
        at = @At(value = "RETURN"),
        method = "createArrow(Lnet/minecraft/world/World;Lnet/minecraft/entity/LivingEntity;Lnet/minecraft/item/ItemStack;Lnet/minecraft/item/ItemStack;)Lnet/minecraft/entity/projectile/PersistentProjectileEntity;",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    private static void biom4st3r_onCrossbowArrowCreation(World world, LivingEntity entity, ItemStack crossbow, ItemStack arrow, CallbackInfoReturnable<PersistentProjectileEntity> ci) {
        EnchantmentHelper.get(crossbow).forEach((enchant,lvl) -> {
            ExtendedEnchantment ex = (ExtendedEnchantment) enchant;
            if (ex.isExtended()) {
                if (ex.transfersFromCrossbowToArrow()) {
                    EnchantableProjectileEntity.enchantProjectile(ci.getReturnValue(), enchant, lvl);
                }
            }
        });
        OnCrossBowArrowCreationEvent.EVENT.invoker().onCreation(ci.getReturnValue(), crossbow, arrow, entity);
    }
}