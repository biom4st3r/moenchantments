package com.biom4st3r.moenchantments.mixin.alphafire;

import java.util.List;

import com.biom4st3r.moenchantments.logic.AlphafireLogic;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

@Mixin(Block.class)
public abstract class BlockMixin {
    /**
     * 
     * @whyhere This is the final method before the list of dropped items is converted to entities
     */
    @Inject(
        at = @At("RETURN"), 
        method = "getDroppedStacks(Lnet/minecraft/block/BlockState;Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/entity/BlockEntity;Lnet/minecraft/entity/Entity;Lnet/minecraft/item/ItemStack;)Ljava/util/List;", 
        cancellable = false
    )
    private static void biom4st3r_moenchantments_doAlphaFire(BlockState state, ServerWorld world, BlockPos pos,
            BlockEntity blockEntity, Entity entity, ItemStack tool, CallbackInfoReturnable<List<ItemStack>> ci) {
        if (EnchantmentRegistry.AUTOSMELT.hasEnchantment(tool)) {
            // if(ItemStack.areEqual(tool, entity.))
            // ServerPlayerInteractionManager#tryBreak passes a copy of the actual item. that copy eventually reaches this methods. So any modifications to this tool are nil...unless you just get mainStack and pass that
            // TODO somehow check if mainstack is the original copy of tool or if it's the offhand
            AlphafireLogic.attemptSmeltStacks(world, entity, entity instanceof PlayerEntity ? ((PlayerEntity)entity).getMainHandStack() : tool, ci.getReturnValue());
        }
    }
}