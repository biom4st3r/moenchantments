package com.biom4st3r.moenchantments.mixin.grapnel;

import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.ProjectileEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.projectile.PersistentProjectileEntity;

import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;
import com.biom4st3r.moenchantments.logic.GrapnelLogic;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(ProjectileEntityRenderer.class)
public abstract class ProjectileEntityRendererMxn {

    @Inject(
        at = @At(value = "TAIL"), 
        method = "render", 
        cancellable = false, 
        locals = LocalCapture.NO_CAPTURE)
    public void renderLineTo(PersistentProjectileEntity persistentProjectileEntity, float yaw, float tickDelta,
            MatrixStack stack, VertexConsumerProvider vertexConsumerProvider, int light, CallbackInfo ci) {

        if (persistentProjectileEntity.getOwner() != null) {
            if (((EnchantableProjectileEntity) persistentProjectileEntity).getEnchantmentLevel(EnchantmentRegistry.GRAPNEL) != 0) {
                if (!((PersistentProjectileAccessor)persistentProjectileEntity).isInGround()) {
                    GrapnelLogic.biom4st3r_renderLineToOwner(persistentProjectileEntity, stack, vertexConsumerProvider, tickDelta);
                }
            }
        }
    }


}