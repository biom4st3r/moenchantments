package com.biom4st3r.moenchantments.mixin.potion_retention;

import java.util.List;
import java.util.stream.Collectors;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

import com.biom4st3r.moenchantments.logic.RetainedPotion;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Local;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.util.math.random.Random;

@Mixin({EnchantmentHelper.class})
public class EnchantmentHelperMixin {
    // TODO Candidate for moenchant-lib
    @WrapOperation(
        // there is an F_NEW immediately after this call. Shift.AFTER moves to after F_NEW
        // slice = @Slice(
        //     from = @At(
        //         value = "INVOKE", 
        //         target = "Lnet/minecraft/item/ItemStack;addEnchantment(Lnet/minecraft/enchantment/Enchantment;I)V"

        //     ),
        //     to = @At(
        //         value = "JUMP",
        //         opcode = Opcodes.GOTO,
        //         shift = Shift.BEFORE,
        //         ordinal = 1
        //     )
        // ),
        at = @At(
            value = "INVOKE", 
            target = "Lnet/minecraft/item/ItemStack;addEnchantment(Lnet/minecraft/enchantment/Enchantment;I)V"

        ),
        method = "enchant"
    )
    private static void moenchantment$enchantmentRandomlyAppliedToTool(ItemStack host, Enchantment e, int lvl, 
        Operation<Void> ori, @Local(index=0) Random random, @Local(print = false, index = 7) EnchantmentLevelEntry entry) {

        ori.call(host, e,lvl);
        if (entry.enchantment == EnchantmentRegistry.POTIONRETENTION) {
            RetainedPotion.borrowRetainedPotion(host, rt -> {
                List<StatusEffect> ses = Registries.STATUS_EFFECT
                    .stream()
                    .filter(s->!s.isBeneficial())
                    .collect(Collectors.toList());
                StatusEffect se = ses.get(random.nextInt(ses.size()));
                rt.setPotionEffectAndCharges(new StatusEffectInstance(se, random.nextBetween(100, 300), 1));
            });
        }
    }
}
