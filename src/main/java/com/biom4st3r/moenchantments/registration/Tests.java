package com.biom4st3r.moenchantments.registration;

import java.util.UUID;

import com.biom4st3r.moenchantments.util.TestUtil;
import com.mojang.authlib.GameProfile;

import net.fabricmc.fabric.api.gametest.v1.FabricGameTest;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.test.GameTest;
import net.minecraft.test.TestContext;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;

public class Tests implements FabricGameTest {

    /*
    TAMEDPROTECTION
    POTIONRETENTION
    ARROW_CHAOS
    GRAPNEL
    SLIPPERY
    FILTER_FEEDER
    VOID_STEP
    enchantment

    TestCreated
        TREEFELLER
        VEINMINER
        AUTOSMELT
        BLACK_HOLE
        ENDERPROTECTION
    Untestable:
        BOW_ACCURACY
        BOW_ACCURACY_CURSE
        SOULBOUND
    */

    @SafeVarargs
    private static ItemStack createStackWithEnchants(Item i, Object... enchantments) {
        ItemStack is = new ItemStack(i);
        for(EnchantmentLevelEntry entry : ofEnchants(enchantments)) {
            is.addEnchantment(entry.enchantment, entry.level);
        }
        return is;
    }

    private static EnchantmentLevelEntry[] ofEnchants(Object... objs) {
        EnchantmentLevelEntry[] array = new EnchantmentLevelEntry[objs.length / 2];
        for(int i = 0; i < array.length; i++) {
            array[i] = new EnchantmentLevelEntry((Enchantment)objs[i*2], (Integer)objs[i*2+1]);
        }
        return array;
    }
        
    private static PlayerEntity createPlayer(TestContext context) {
        return new PlayerEntity(context.getWorld(), BlockPos.ORIGIN, 0.0F, new GameProfile(UUID.randomUUID(), "test-mock-player")) {
            @Override
            public boolean isCreative() {
                return false;
            }

            @Override
            public boolean isSpectator() {
                return false;
            }
        };
    }

    public static ServerPlayerEntity createServerPlayer(TestContext ctx) {
        return TestUtil.createServerPlayer(ctx.getWorld());
    }

    @GameTest(
        required = true, 
        maxAttempts = 3, 
        templateName = "moenchantments:test_mining"
    )
    public static void test_mining_all(TestContext context) {
        // PlayerEntity player = createPlayer(context);
        ServerPlayerEntity player = createServerPlayer(context);
        ItemStack pick = createStackWithEnchants(Items.NETHERITE_PICKAXE, 
            EnchantmentRegistry.VEINMINER, 3,
            EnchantmentRegistry.AUTOSMELT, 1,
            EnchantmentRegistry.BLACK_HOLE, 1,
            Enchantments.FORTUNE, 3
            );

        player.setStackInHand(Hand.MAIN_HAND, pick);
        player.interactionManager.tryBreakBlock(context.getAbsolutePos(new BlockPos(0,2,0)));
        // BlockBreakEvent.run(player.world, context.getAbsolutePos(new BlockPos(0,2,0)), player);
        int netherScrapCount = player.getInventory().count(Items.NETHERITE_SCRAP);
        int ancientDebrisCount = player.getInventory().count(Items.ANCIENT_DEBRIS);
        if(netherScrapCount == 0) {
            context.throwGameTestException("Blackhole didn't add items to inventory");
        } else if(netherScrapCount == 1) {
            context.throwGameTestException("Veinminer didn't run");
        } else if(ancientDebrisCount > 0) {
            context.throwGameTestException("Alphafire failed");
        } else if(netherScrapCount <= 16) {
            context.throwGameTestException("Fortune failed");
        }

        context.complete();
    }

    @GameTest(
        required = true, 
        maxAttempts = 1, 
        templateName = "moenchantments:tree_feller_test"
    )
    public static void test_tree_feller(TestContext context) {
        // PlayerEntity player = createPlayer(context);
        ServerPlayerEntity player = createServerPlayer(context);
        ItemStack pick = createStackWithEnchants(Items.NETHERITE_AXE, 
            EnchantmentRegistry.TREEFELLER, 3,
            EnchantmentRegistry.AUTOSMELT, 1,
            EnchantmentRegistry.BLACK_HOLE, 1
            );

        player.setStackInHand(Hand.MAIN_HAND, pick);
        // BlockBreakEvent.run(player.world, context.getAbsolutePos(new BlockPos(2,2,2)), player);

        player.interactionManager.tryBreakBlock(context.getAbsolutePos(new BlockPos(2,2,2)));
        int charcoal = player.getInventory().count(Items.CHARCOAL);
        int log = player.getInventory().count(Items.ACACIA_LOG);
        if(charcoal == 0) {
            context.throwGameTestException("Blackhole didn't add items to inventory");
        } else if(charcoal == 1) {
            context.throwGameTestException("Treefeller didn't run");
        } else if(log > 0) {
            context.throwGameTestException("Alphafire failed");
        }

        context.complete();
    }

    @GameTest(
        required = true,
        maxAttempts = 3,
        templateName = "moenchantments:test_enderprotection",
        tickLimit = 60,
        requiredSuccesses = 1
    )
    public static void test_enderprotection(TestContext context) {
        PlayerEntity player = createPlayer(context);
        BlockPos rel = new BlockPos(5, 2, 5);
        BlockPos dest = context.getAbsolutePos(rel);

        // player.teleport(dest.getX(),dest.getY(),dest.getZ());
        player.refreshPositionAndAngles(dest.getX(), dest.getY(), dest.getZ(), 0, 0);
        player.setPos(dest.getX(), dest.getY(), dest.getZ());

        ItemStack stack = createStackWithEnchants(Items.LEATHER_HELMET, EnchantmentRegistry.ENDERPROTECTION, 1);

        player.equipStack(EquipmentSlot.HEAD, stack);
        
        context.getWorld().spawnEntity(player);
        ArrowEntity arrow = context.spawnEntity(EntityType.ARROW, rel.add(0, 3, 0));

        context.runAtTick(context.getTick()+40, () -> {
                ArrowEntity ar = arrow;
                PlayerEntity pe = player;
                double distance = pe.squaredDistanceTo(ar);
                if(distance > 3*3) {
                    context.complete();
                } else {
                    context.throwGameTestException("teleport_failed");
                }
        });
    }

    static {
        //     EnchantmentRegistry.TREEFELLER,EnchantmentRegistry.VEINMINER,EnchantmentRegistry.ENDERPROTECTION,EnchantmentRegistry.AUTOSMELT,EnchantmentRegistry.BLACK_HOLE,
        // Object o = new Enchantment[] {
        //     EnchantmentRegistry.TAMEDPROTECTION,
        //      EnchantmentRegistry.SOULBOUND,
        //     EnchantmentRegistry.POTIONRETENTION, EnchantmentRegistry.BOW_ACCURACY,
        //     EnchantmentRegistry.BOW_ACCURACY_CURSE, EnchantmentRegistry.ARROW_CHAOS,
        //     EnchantmentRegistry.GRAPNEL,
        //     EnchantmentRegistry.SLIPPERY,EnchantmentRegistry.VOID_STEP,
        //     EnchantmentRegistry.enchantment,
        // };
    }
}
