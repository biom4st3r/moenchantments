package com.biom4st3r.moenchantments.registration;

public final class Graveyard {

    // // KORR Issue #48
    // // Chance to knock an item out of a mob/players hand
    // static final ExtendedEnchantment DISARM = EnchantBuilder.newBuilder(Rarity.UNCOMMON, HAND_SLOTS)
    //     .maxlevel(1)
    //     .isAcceptible(isTool)
    //     .enabled(false)
    //     .buildAndRegister("disarm");

    // // When being repaired in any capcity there is a chance to remove a random enchantment including itself
    // // KORR Issue #48 Items
    // static final ExtendedEnchantment DISENCHANTMENT = EnchantBuilder.newBuilder(Rarity.VERY_RARE, ALL_SLOTS)
    //     .maxlevel(3) // Weakly Magical, Barely Magical, Not Magical
    //     .isAcceptible(YES)
    //     .enabled(false)
    //     .buildAndRegister("disenchantment");

    // static boolean supportsMagnetism(Ingredient ingredient) {
    //     Object[] entries = INGREDIENT$entries.get(ingredient);
    //     Item repairItem = INGREDIENT$ENTRY$getStacks.invoke(entries[0]).iterator().next().getItem();
    //     return equalsAny(repairItem, Items.IRON_INGOT, Items.GOLD_INGOT, Items.COPPER_INGOT, Items.NETHERITE_INGOT);
    // }
    
    // // When thrown the item is draw to any ore of the same type within a reasonable distance. Doesn't work for gems.
    // static final ExtendedEnchantment MAGNETISM = EnchantBuilder.newBuilder(Rarity.RARE, ALL_SLOTS)
    //     .maxlevel(1)
    //     .isAcceptible(s -> isArmor.and(stack -> {
    //         ArmorItem item = (ArmorItem) stack.getItem();
    //         return supportsMagnetism(item.getMaterial().getRepairIngredient());
    //     }).test(s) ||
    //         isTool.and(stack -> {
    //             ToolItem item = (ToolItem) stack.getItem();
    //             return supportsMagnetism(item.getMaterial().getRepairIngredient());
    //         }).test(s))
    //     .enabled(FabricLoader.getInstance().isDevelopmentEnvironment())
    //     .buildAndRegister("magnetism");
// public static final EnchantmentSkeleton BUILDERS_WAND = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.HAND_SLOTS)
//     .treasure(true)
//     .minpower(level -> 10)
//     .enabled(false)
//     .isAcceptible(itemstack -> itemstack.getItem() == Items.STICK)
//     .build("builders_wand");
//     //https://www.reddit.com/r/minecraftsuggestions/comments/dx8zom/shield_enchantments/
// public static final EnchantmentSkeleton SHIELD_REFLECT = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, EquipmentSlot.OFFHAND)
//     .maxlevel(3)
//     .treasure(true)
//     .enabled(false)
//     .build("reflectarrow");
// public static final EnchantmentSkeleton TRAINING_WEAPON = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
//     .maxlevel(1)
//     .treasure(true)
//     .enabled(false)//MoEnchantsConfig.config.EnableTrainingWeapon)
//     .build("training_weapon"); // Provide more xp from kill. but reduce damage;
// public static final EnchantmentSkeleton VAMPIRISM = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
//     .treasure(true)
//     .minpower(level ->  10)
//     .maxpower(level -> 15)
//     .enabled(false)//MoEnchantsConfig.config.EnableVampirism)
//     .build("vampirism");
// public static final EnchantmentSkeleton HOARDING = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
//     .curse(true)
//     .minpower(level -> 20)
//     .maxpower(level -> 30)
//     .enabled(false)//MoEnchantsConfig.config.EnableHording)
//     .build("hording");
// public static final EnchantmentSkeleton SHIELD_BASH = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.HAND_SLOTS)
//     .treasure(true)
//     .minpower(level -> 10)
//     .enabled(false)
//     .addExclusive(SHIELD_REFLECT)
//     .build("shield_bash");
// public static final EnchantmentSkeleton TELEPORTING_ARROW = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
//     .treasure(true)
//     .minpower(l -> 30)
//     .enabled(false)
//     .addExclusive(Enchantments.MULTISHOT).addExclusive(Enchantments.INFINITY).addExclusive(EnchantmentRegistry.ARROW_CHAOS)
//     .build("teleport_arrow");
// public static final EnchantmentSkeleton ARROW_DISTANCE = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
//     .minpower(level -> 8)
//     .enabled(false)
//     .build("oomph");

    // // Load multiple arrow into a crossbow
    // public static final ExtendedEnchantment MULTI_LOAD = EnchantBuilder.newBuilder(Rarity.UNCOMMON, EquipmentSlot.MAINHAND)
    //     .maxlevel(2)
    //     .enabled(false)
    //     .isAcceptible(isCrossbow)
    //     .treasure(true)
    //     .applyEnchantmentToArrowsFromCrossbows()
    //     .buildAndRegister("multiload");

    // // Full density fully reflects arrows and Tridents. Higher levels increase the chance of damaging attacking weapons or dropping the attacking item
    // static final ExtendedEnchantment DENSITY = EnchantBuilder.newBuilder(Rarity.VERY_RARE, ARMOR_SLOTS)
    //     .maxlevel(1)
    //     .isAcceptible(isArmor)
    //     .treasure(true)
    //     .enabled(false)
    //     .buildAndRegister("densearmor");
    // // Can eat when full
    // static final ExtendedEnchantment GLUTEN = EnchantBuilder.newBuilder(Rarity.UNCOMMON, EquipmentSlot.CHEST)
    //     .maxlevel(1)
    //     .isAcceptible(isChest)
    //     .treasure(true)
    //     .enabled(false)
    //     // .addExclusive(DISCOMFORT) // a chestplate can't be constrictive and expansive
    //     .buildAndRegister("gluten");
    // // Pulls all nearby entity towards the landing arrow. ?Impodes/deals massive damage to hit target?
    // static final ExtendedEnchantment IMPLODE = EnchantBuilder.newBuilder(Rarity.RARE, HAND_SLOTS)
    //     .maxlevel(1)
    //     .isAcceptible(isBow.or(isCrossbow))
    //     .minpower(lvl -> 22)
    //     .maxpower(lvl -> 27)
    //     .addExclusive(GRAPNEL)
    //     .treasure(true)
    //     .enabled(false)
    //     .buildAndRegister("arrow_implode");
    // // KORR Issue #48
    // // ChestPlate less air underwater
    // static final ExtendedEnchantment DISCOMFORT = EnchantBuilder.newBuilder(Rarity.RARE, EquipmentSlot.CHEST)
    //     .maxlevel(1)
    //     .curse(true)
    //     .isAcceptible(isChest)
    //     .treasure(true)
    //     .minpower(lvl -> 12)
    //     .maxpower(lvl -> 20)
    //     .enabled(false)
    //     .buildAndRegister("discomfort");
}