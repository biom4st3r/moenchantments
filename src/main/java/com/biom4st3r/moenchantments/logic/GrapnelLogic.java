package com.biom4st3r.moenchantments.logic;

import java.util.Set;

import org.joml.Matrix4f;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.google.common.collect.Sets;

import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;
import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.LightType;

public final class GrapnelLogic {
    public static final Set<EntityType<?>> VALID_ARROW_ENTITY = Sets.newHashSet(EntityType.ARROW, EntityType.SPECTRAL_ARROW);

    public static void events() {
    }

    public static void biom4st3r_renderLineToOwner(PersistentProjectileEntity arrow, MatrixStack stack, VertexConsumerProvider vertexConsumerProvider, float tickDelta) {
        Entity shooter = arrow.getOwner();
        stack.push();
        double oneDegreeRadians = Math.PI / 180.0D;
        double nextYawShooter = MathHelper.lerp(tickDelta * 0.5F, shooter.getYaw(), shooter.prevYaw) * oneDegreeRadians;
        double nextPitchShooter = MathHelper.lerp(tickDelta * 0.5F, shooter.getPitch(), shooter.prevPitch) * oneDegreeRadians;
        
        // Offset the rope position to the right side
        double cosYaw = Math.cos(nextYawShooter);
        double sinyaw = Math.sin(nextYawShooter);
        double sinPitch = Math.sin(nextPitchShooter);
        double cosPitch = Math.cos(nextPitchShooter);
        EntityType.ARROW.hashCode();
        // InlinedValue
        double someRadian = 0.7D;

        double nextShooterX = MathHelper.lerp(tickDelta, shooter.prevX, shooter.getX()) - cosYaw * someRadian
                - sinyaw * 0.5D * cosPitch;
        double nextShooterY = MathHelper.lerp(tickDelta, shooter.prevY + shooter.getStandingEyeHeight() * someRadian,
                shooter.getY() + shooter.getStandingEyeHeight() * someRadian) - sinPitch * 0.5D - 0.25D;
        double nextShooterZ = MathHelper.lerp(tickDelta, shooter.prevZ, shooter.getZ()) - sinyaw * someRadian
                + cosYaw * 0.5D * cosPitch;

        // Inlined
        double piOver2 = Math.PI / 2.0D;
        
        double nextYawArrow = (MathHelper.lerp(tickDelta, arrow.getYaw(), arrow.prevYaw) * oneDegreeRadians) + piOver2;
        //Keep this comment
        // Vec3d standingHeight = arrow.getLeashOffset();
        Vec3d standingHeight = new Vec3d(0.0, arrow.getStandingEyeHeight(), arrow.getWidth() * 0.4F);
        cosYaw = Math.cos(nextYawArrow) * standingHeight.z + Math.sin(nextYawArrow) * standingHeight.x;
        sinyaw = Math.sin(nextYawArrow) * standingHeight.z - Math.cos(nextYawArrow) * standingHeight.x;
        double nextXArrow = MathHelper.lerp(tickDelta, arrow.prevX, arrow.getX()) + cosYaw;
        double nextYArrow = MathHelper.lerp(tickDelta, arrow.prevY, arrow.getY()) + standingHeight.y;
        double nextZArrow = MathHelper.lerp(tickDelta, arrow.prevZ, arrow.getZ()) + sinyaw;
        stack.translate(cosYaw, standingHeight.y, sinyaw);
        float deltaX = (float) (nextShooterX - nextXArrow);
        float deltaY = (float) (nextShooterY - nextYArrow);
        float deltaZ = (float) (nextShooterZ - nextZArrow);
        
        //Inlined
        float unknownValue = 0.025F;
        VertexConsumer vc = vertexConsumerProvider.getBuffer(RenderLayer.getLightning());
        
        Matrix4f matrix = stack.peek().getPositionMatrix();
        float horizOffset = MathHelper.fastInverseSqrt(deltaX * deltaX + deltaZ * deltaZ) * unknownValue / 2.0F;
        float zOffset = deltaZ * horizOffset;
        float xOffset = deltaX * horizOffset;
        BlockPos blockPos = new BlockPos(arrow.getCameraPosVec((float)tickDelta));
        BlockPos blockPos2 = new BlockPos(shooter.getCameraPosVec((float)tickDelta));
        int arrowLight = arrow.getEntityWorld().getLightLevel(new BlockPos(arrow.getPos()));// 0xF000F0;//this.getBlockLight(mobEntity,
                                                                                            // blockPos);
        int shooterLight = shooter.getEntityWorld().getLightLevel(new BlockPos(shooter.getPos()));// 0xF000F0;//this.dispatcher.getRenderer(entity).getBlockLight(entity,
                                                                                                  // blockPos2);
        int arrowSkyLight = arrow.world.getLightLevel(LightType.SKY, blockPos);
        int shooterSkyLight = arrow.world.getLightLevel(LightType.SKY, blockPos2);
        createQuads(vc, matrix, deltaX, deltaY, deltaZ, arrowLight, shooterLight, arrowSkyLight, shooterSkyLight,
                unknownValue, unknownValue, zOffset, xOffset);
        createQuads(vc, matrix, deltaX, deltaY, deltaZ, arrowLight, shooterLight, arrowSkyLight, shooterSkyLight, unknownValue, 0.0F, zOffset, xOffset);
        stack.pop();
    }

    private static void createQuads(VertexConsumer vertexConsumer, Matrix4f matrix4f, float deltaX, float deltaY,
            float deltaZ, int arrowLight, int shooterLight, int arrowSkyLight, int shooterSkyLight, float m, float n,
            float zOffset, float xOffset) {
        // int q = true;
        int max = 24;
        for (int vIndex = 0; vIndex < max; ++vIndex) {
            float s = (float) vIndex / (max - 1);
            int avgLight = (int) MathHelper.lerp(s, (float) arrowLight, (float) shooterLight);
            int avgSkyLight = (int) MathHelper.lerp(s, (float) arrowSkyLight, (float) shooterSkyLight);
            int light = LightmapTextureManager.pack(avgLight, avgSkyLight);
            createVertex(vertexConsumer, matrix4f, light, deltaX, deltaY, deltaZ, m, n, max, vIndex, false, zOffset,
                    xOffset);
            createVertex(vertexConsumer, matrix4f, light, deltaX, deltaY, deltaZ, m, n, max, vIndex + 1, true, zOffset,
                    xOffset);
        }

    }

    private static void createVertex(VertexConsumer vc, Matrix4f matrix, int light, float deltaX, float deltaY,
            float deltaZ, float j, float k, int loopMax, int vertexIndex, boolean bl, float zOffset, float xOffset) {
        float red = 0.2F;
        float green = 0.4F;
        float blue = 0.7F;
        if (vertexIndex % 2 == 0) {
            red *= 0.2F;
            green *= 0.2F;
            blue *= 0.2F;
        }

        float a = (float) vertexIndex / (float) loopMax;
        float xPos = deltaX * a;
        float yPos = deltaY > 0.0F ? deltaY * a * a : deltaY - deltaY * (1.0F - a) * (1.0F - a);
        float zPos = deltaZ * a;
        // float yMod = 0;//(float)Math.sin(vertexIndex) * (1F / (float)Math.sqrt(vertexIndex + 1));// * (vertexIndex = =loopMax ? 0 : 1);
        if (!bl) {
            vc.vertex(matrix, xPos + zOffset, (yPos + j - k) , zPos - xOffset).texture(0, 0).overlay(0, 0).color(red, green, blue, 1.0F).light(light)
                    .next();
        }
        vc.vertex(matrix, xPos - zOffset, (yPos + k), zPos + xOffset).texture(0, 0).overlay(0, 0).color(red, green, blue, 1.0F).light(light)
            .next();
        if (bl) {
            vc.vertex(matrix, xPos + zOffset, (yPos + j - k) , zPos - xOffset).texture(0, 0).overlay(0, 0).color(red, green, blue, 1.0F).light(light)
                .next();
        }
    }

    public static void applyVelocity(Entity target, Entity shooter) {
        if (target.world.isClient) return;
        double distance = target.getPos().distanceTo(shooter.getPos());
        double deltaX = shooter.getX() - target.getX();
        double deltaY = shooter.getY() - target.getY();
        double deltaZ = shooter.getZ() - target.getZ();

        double multiplier = 1D / Math.sqrt(distance);
        Vec3d velocity = new Vec3d(deltaX, Math.max(deltaY, 1D), deltaZ).multiply(multiplier, multiplier, multiplier).multiply(1.17D, 0.6D, 1.17D);
        if (MoEnchantsConfig.config.ReduceElytraVelocityWithGrapnel) {
            if (target instanceof PlayerEntity && ((PlayerEntity)target).isFallFlying()) {
                velocity.multiply(MoEnchantsConfig.config.ReduceElytaVelocityWithGrapnelMultiplier);
            }
        }
        
        ModInit.logger.debug("%s", velocity.toString());
        target = target.getRootVehicle();
        target.addVelocity(velocity.x, velocity.y, velocity.z);
        target.velocityDirty = true;
        target.velocityModified = true;
        if (shooter instanceof PersistentProjectileEntity arrow) {
            ((EnchantableProjectileEntity)arrow).setEnchantmentLevel(EnchantmentRegistry.GRAPNEL, 0);
        }
        
    }


}