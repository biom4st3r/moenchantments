package com.biom4st3r.moenchantments.entities;

import java.lang.invoke.MethodHandle;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.jetbrains.annotations.Nullable;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;
import com.biom4st3r.moenchantments.util.TagHelper;
import com.biom4st3r.moenchantments.util.TestUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import biom4st3r.libs.biow0rks.BioLogger;
import biom4st3r.libs.biow0rks.NoEx;
import biom4st3r.libs.biow0rks.reflection.GodMode;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.BarrelBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ChestBlock;
import net.minecraft.block.ShulkerBoxBlock;
import net.minecraft.block.StemBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.ai.goal.WanderAroundGoal;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtOps;
import net.minecraft.registry.Registries;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.IntProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class LivingItemEntity extends TameableEntity implements Inventory {
    private static final TrackedData<ItemStack> STACK = DataTracker.registerData(LivingItemEntity.class, TrackedDataHandlerRegistry.ITEM_STACK);
    private static final TrackedData<Integer> EMOTE = DataTracker.registerData(LivingItemEntity.class, TrackedDataHandlerRegistry.INTEGER);
    private static final TrackedData<Optional<BlockState>> HAS_BACKPACK = DataTracker.registerData(LivingItemEntity.class, TrackedDataHandlerRegistry.OPTIONAL_BLOCK_STATE);
    private static final int MAX_FULL_AREA_SCAN = 200;
    private static final int TPS = 20;
    private static final int EMOTE_DURATION = 20*3;
    public static final int SPAWN_WITH_VILLAGER_CHANCE = 15;
    public static final int SPAWN_WITH_PATROL_CHANCE = 3;

    private static final boolean FLAGS = Calendar.getInstance().get(Calendar.MONTH) == 6 || FabricLoader.getInstance().isDevelopmentEnvironment();

    /**Collection of valid Blockstate targets. Initilized in ctor. */
    private Set<BlockState> TARGETS = null;
    /** Informs the Goal when it can start and what it should do. */
    MiningInfo info = new MiningInfo();
    /** Remaining time for the currently displaying emote */
    private int emoteTimer = 0;
    /** Dummy BlockEntity used for rendering */
    BlockEntity be = null;
    /** inventory used for Enchantment Lvl 2 */
    SimpleInventory inventory = new SimpleInventory(7);
    /** Whether or not the owner is a regular player or entity. */
    private boolean ownerIsEntity = false;

    public LivingItemEntity(World world) {
        super(ModInit.LIVING_ITEM_TYPE, world);
        this.experiencePoints = 0;
        this.collectTargets();
    }

    public LivingItemEntity(World world, ItemStack stack) {
        this(world);
        this.setStack(stack);
    }

    public LivingItemEntity(World world, ItemStack stack, LivingEntity owner) {
        this(world, stack);
        this.setOwner(owner);
    }

    public void setOwner(LivingEntity entity) {
        if (entity instanceof PlayerEntity pe) {
            this.setOwner(pe);
            ownerIsEntity = false;
        } else {
            this.setOwnerUuid(entity.getUuid());
            this.setTamed(true);
            ownerIsEntity = true;
        }
    }

    @Override
    public LivingEntity getOwner() {
        if (ownerIsEntity) {
            List<LivingEntity> list = this.world.getEntitiesByClass(LivingEntity.class, this.getBoundingBox().expand(20, 10, 20), e -> e.getUuid().equals(this.getOwnerUuid()));
            if (list.size() != 0) {
                return list.get(0);
            }
            return null;
        }
        return super.getOwner();
    }

    public static final MethodHandle SimpleInventory$stacks = NoEx.run(()->GodMode.GOD.unreflectGetter(Stream.of(SimpleInventory.class.getDeclaredFields()).filter(f -> f.getType() == DefaultedList.class).findFirst().get()));

    /** creates a LivingItemEntity from itemEntity. Usually called when an Item is dropped. */
    public static LivingItemEntity create(ItemEntity itemEntity) {
        if (itemEntity.world.isClient) return null;
        if (itemEntity.getThrower() == null) return null;
        if (!EnchantmentRegistry.LIFE_LIKE.isEnabled()) return null;
        LivingItemEntity entity = new LivingItemEntity(itemEntity.world, itemEntity.getStack());
        entity.refreshPositionAndAngles(itemEntity.getBlockPos(), itemEntity.getYaw(), itemEntity.getPitch());
        if (itemEntity.getThrower() != null) {
            entity.setOwnerUuid(itemEntity.getThrower());
            entity.setTamed(true);
        }
        return entity;
    }

    /** Ticks down the current emote being displaced. */
    private void emoteTick() {
        if (emoteTimer > 0)  {
            if (--emoteTimer == 0) {
                this.setEmote(Emote.NONE);
            }
        }
    }

    public void breakBlockAsPlayer(BlockPos pos) {
        if (this.world.isClient) return;
        // TODO This is bad wasteful. maybe weakref?
        ServerPlayerEntity DUMMY = TestUtil.createServerPlayer((ServerWorld) this.world);
        DUMMY.refreshPositionAndAngles(this.getBlockPos(), this.getYaw(), this.getPitch());
        DUMMY.setStackInHand(Hand.MAIN_HAND, this.getStack());
        DUMMY.interactionManager.tryBreakBlock(pos);
        DUMMY.setStackInHand(Hand.MAIN_HAND, ItemStack.EMPTY);
        DUMMY.getInventory().dropAll();
    }

    @Override
    public boolean canBreatheInWater() {
        return true;
    }


    @Override
    public boolean canPickUpLoot() {
        return true;
    }

    @Override
    public boolean canGather(ItemStack stack) {
        if (inventory.canInsert(stack) && this.hasBackpack()) {
            return true;
        } else if (this.hasBackpack()) {
            this.setEmote(Emote.WEARY);
            return false;
        }
        return false;
    }

    @Override
    protected void loot(ItemEntity item) {
        ItemStack stack = inventory.addStack(item.getStack());
        this.sendPickup(item, stack.isEmpty() ? item.getStack().getCount() : item.getStack().getCount() - stack.getCount());
        if (stack.isEmpty()) item.discard();
    }

    private static boolean filter(ItemStack stack, BlockState block) {
        return stack.isSuitableFor(block);
    }

    private boolean isPick, isAxe, isShovel, isHoe, isSword;
    private static List<IntProperty> PROPERTIES = Lists.newArrayList(Properties.AGE_1,Properties.AGE_2, Properties.AGE_3, Properties.AGE_5, Properties.AGE_7, Properties.AGE_15, Properties.AGE_25);
    /** Finds all valid Blockstates for the tool */
    private void collectTargets() {
        ItemStack stack = this.getStack();
        this.TARGETS = Sets.newHashSet();
        BiFunction<ItemStack, Stream<BlockState>,Set<BlockState>> func = (is,stream) -> {
            return stream
                .filter(b -> {
                    return filter(stack, b);
                })
                .collect(Sets::newHashSet, HashSet::add, HashSet::addAll);
        };
        isPick = stack.isSuitableFor(Blocks.STONE.getDefaultState());
        isAxe = stack.isSuitableFor(Blocks.OAK_LOG.getDefaultState());
        isShovel = stack.isSuitableFor(Blocks.DIRT.getDefaultState());
        isHoe = stack.isSuitableFor(Blocks.SPONGE.getDefaultState());
        isSword = stack.isSuitableFor(Blocks.COBWEB.getDefaultState());

        if (isPick) {
            this.TARGETS.addAll(func.apply(this.getStack(), TagHelper.toStream(Registries.BLOCK, TagHelper.block("moenchantments:ores")).map(b -> b.getDefaultState())));
            if (this.getStack().isSuitableFor(Blocks.RAW_IRON_BLOCK.getDefaultState())) this.TARGETS.add(Blocks.RAW_IRON_BLOCK.getDefaultState());
        }
        if (isAxe) {
            // TODO Melons and Pumpkins
            this.TARGETS.addAll(func.apply(this.getStack(), TagHelper.toStream(Registries.BLOCK, BlockTags.LOGS).map(b -> b.getDefaultState())));
        }
        if (isShovel) {
            // Not yet
        }
        if (isHoe) {
            TagHelper.toStream(Registries.BLOCK, BlockTags.CROPS)
                .map(b -> b.getDefaultState())
                .map(state -> {
                    for (IntProperty x : PROPERTIES) {
                        if (state.contains(x)) {
                            return state.with(x, x.getValues().stream().mapToInt(i -> i.intValue()).max().getAsInt());
                        }
                    }
                    return null;
                })
                .filter(s -> s != null && !(s.getBlock() instanceof StemBlock))
                .forEach(this.TARGETS::add)
            ;
            // TODO Netherwarts
        }
        if (isSword) {
        }
    }
    /** If a stack is valid for the enchantment. */
    public static Predicate<ItemStack> IS_ACCEPTIBLE = stack -> {
        return stack.isSuitableFor(Blocks.STONE.getDefaultState()) || stack.isSuitableFor(Blocks.OAK_LOG.getDefaultState()) || stack.isSuitableFor(Blocks.SPONGE.getDefaultState());
    };

    public final class MiningInfo {
        public MiningInfo() {
            init();
        }
        public void init() {
            shouldMineOre = false;
            targetOrePos = BlockPos.ORIGIN;
            targetOre = Blocks.AIR.getDefaultState();
        }
        public boolean shouldMineOre;
        public BlockPos targetOrePos;
        public BlockState targetOre;
    }

    @Override
    protected void initGoals() {
        this.goalSelector.add(1, new FindOreGoal2(this));
        this.goalSelector.add(2, new FollowOwnerGoal(this, 1.0, 10.0f, 2.0f, false) {
            @Override
            public void start() {
                setEmote(Emote.PANIC);
                super.start();
            }
        });
        this.goalSelector.add(3, new LookAtEntityGoal(this, LivingEntity.class, 8.0f) {
            @Override
            public void start() {
                super.start();
                if (random.nextInt(10) == 0) {
                    if (FLAGS) setEmote(Emote.values()[5+random.nextInt(3)]);
                    else setEmote(Emote.WAVE);
                }
            }
            @Override
            public boolean canStart() {
                return super.canStart() && !info.shouldMineOre; //  && !info.shouldMineOre
            }
        });
        this.goalSelector.add(4, new LookAroundGoal(this) {
            @Override
            public boolean canStart() {
                return super.canStart() && !info.shouldMineOre; //  
            }
        });
        this.goalSelector.add(5, new WanderAroundGoal(this, 1.0));
        // this.goalSelector
        // this.goalSelector.add(6, new WanderNearTargetGoal(mob, speed, maxDistance));
    }

    private static BioLogger LOGGER = new BioLogger("LIE");

    /** Searches through the specified area arround the entity. Potentially triggering the mining goal */
    void searchForOre(int xpand, int ypand, int zpand) {
        if (this.world.isClient) return;
        int area = (xpand*2) * (ypand*2) * (zpand*2);
        Box box = new Box(this.getBlockPos()).expand(xpand, ypand, zpand);
        LOGGER.debug("Searching for ore: %s cubic meters", area);
        Iterable<BlockPos> iter = area > MAX_FULL_AREA_SCAN 
            ? BlockPos.iterateRandomly(this.random, area / 2, (int)box.minX, (int)box.minY, (int)box.minZ, (int)box.maxX, (int)box.maxY, (int)box.maxZ) 
            : BlockPos.iterate((int)box.minX, (int)box.minY, (int)box.minZ, (int)box.maxX, (int)box.maxY, (int)box.maxZ);
        for (BlockPos pos : iter) {
            BlockState state = this.world.getBlockState(pos);
            if (TARGETS.contains(state)) {
                LOGGER.debug("FOUND ORE AT %s", pos);
                this.info.shouldMineOre = true;
                this.info.targetOrePos = pos.toImmutable();
                this.info.targetOre = this.world.getBlockState(pos);
                this.setEmote(Emote.VERY_HAPPY);
                // this.world.playSound(null, this.getX(), this.getY(), this.getZ(), SoundEvents.DI, var9, var10, var11);
                break;
            }
        }
    }

    public void spawnStack() {
        if (this.getStack().isEmpty()) return;
        // unbreakable is remove, because this itemstack is used in breakAsPlayer.
        this.getStack().getOrCreateNbt().remove("Unbreakable");
        this.world.spawnEntity(new ItemEntity(world, this.getX(), this.getY(), this.getZ(), this.getStack()));
    }

    @SuppressWarnings({"unchecked"})
    private void spawnBackpack() {
        ItemStack backpack = new ItemStack(this.getDataTracker().get(HAS_BACKPACK).get().getBlock().asItem());
        if (!this.inventory.isEmpty()) {
            NbtCompound nbt = Inventories.writeNbt(new NbtCompound(), (DefaultedList<ItemStack>) NoEx.run(()->SimpleInventory$stacks.invoke(this.inventory)));
            backpack.getOrCreateNbt().put("BlockEntityTag", nbt);
        }
        this.world.spawnEntity(new ItemEntity(world, this.getX(), this.getY(), this.getZ(), backpack));
    }

    private void deathCleanup() {
        if (this.world.isClient) return;
        this.spawnStack();
        if (this.hasBackpack()) {
            this.spawnBackpack();
        }
    }

    @Override
    public ActionResult interactAt(PlayerEntity player, Vec3d hitPos, Hand hand) {
        if (world.isClient) return ActionResult.SUCCESS;
        ItemStack stack = player.getStackInHand(hand);
        Block block = stack.getItem() instanceof BlockItem item ? item.getBlock() : null;
        int level = EnchantmentRegistry.LIFE_LIKE.getLevel(this.getStack());
        final boolean isChestItem = (block instanceof ShulkerBoxBlock || block instanceof ChestBlock || block instanceof BarrelBlock);
        final boolean hasNoData = !(stack.hasNbt() && stack.getNbt().contains("BlockEntityTag"));

        LOGGER.debug("level %d\nchestitem: %s\nnodata: %s", level, isChestItem, hasNoData);
        if (level == 2 && isChestItem && hasNoData) {
            LOGGER.debug("Adding backpack");
            this.setBackpack(block.getDefaultState());
            stack.decrement(1);
            return ActionResult.SUCCESS;
        } else if (player.getUuid().equals(this.getOwnerUuid()) || player.hasPermissionLevel(1) || ownerIsEntity) {
            deathCleanup();
            this.remove(RemovalReason.DISCARDED);
            return ActionResult.SUCCESS;
        }
        return super.interactAt(player, hitPos, hand);
    }

    @Override
    public boolean isPushedByFluids() {
        return false;
    }

    @Override
    public void setCustomName(Text name) {
        super.setCustomName(name);
        this.getStack().setCustomName(name);
    }

    @Override
    public void tick() {
        if (!this.dead && !this.isRemoved() && this.getStack().isEmpty()) {
            this.remove(RemovalReason.DISCARDED);
            LOGGER.error("LivingItemEntity with air stack. Removing.");
        }
        if (this.isAiDisabled()) return;
        if (this.dead) {
            this.deathCleanup();;
            this.setStack(ItemStack.EMPTY);
            super.tick();
            return;
        }
        
        if (this.random.nextInt(TPS * (ownerIsEntity ? 30 : 10)) == 0 && !this.info.shouldMineOre && !this.world.isClient) {
            searchForOre(5, 3, 5);
        }
        emoteTick();
        super.tick();
    }


    @Override
    public PassiveEntity createChild(ServerWorld var1, PassiveEntity var2) {
        return null;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~ DATA TRACKER ~~~~~~~~~~~~~~~~~~~~~~

    @Override
    protected void initDataTracker() {
        super.initDataTracker();
        this.getDataTracker().startTracking(STACK, ItemStack.EMPTY);
        this.getDataTracker().startTracking(EMOTE, 0);
        this.getDataTracker().startTracking(HAS_BACKPACK, Optional.empty());
    }

    public ItemStack getStack() {
        return this.getDataTracker().get(STACK);
    }

    public void setStack(ItemStack stack) {
        if (!stack.isEmpty()) {
            // unbreakable is applied, because this itemstack is used in breakAsPlayer
            stack.getOrCreateNbt().putBoolean("Unbreakable", true);
        }
        if (stack.hasCustomName()) {
            this.setCustomName(stack.getName());
            this.setCustomNameVisible(true);
        }
        this.getDataTracker().set(STACK, stack);
        this.collectTargets();
    }


    public Emote getEmote() {
        return Emote.values()[this.getDataTracker().get(EMOTE)];
    }

    public void setEmote(Emote emote) {
        if (this.getEmote() == emote) return;
        this.getDataTracker().set(EMOTE, emote.ordinal());
        if (emote != Emote.NONE) {
            emoteTimer = EMOTE_DURATION;
            LOGGER.debug("Emote: %s", emote.identifier);
        }
    }

    public void setBackpack(BlockState state) {
        if (state.isAir() || !state.hasBlockEntity()) {
            this.getDataTracker().set(HAS_BACKPACK, Optional.empty());
            this.be = null;
        } else {
            this.getDataTracker().set(HAS_BACKPACK, Optional.of(state));
        }
    }

    public boolean hasBackpack() {
        return this.getDataTracker().get(HAS_BACKPACK).isPresent();
    }

    public BlockEntity getBackpack() {
        if (this.hasBackpack()) {
            if (be == null) {
                BlockState state = this.getDataTracker().get(HAS_BACKPACK).get();
                be = ((BlockEntityProvider)state.getBlock()).createBlockEntity(BlockPos.ORIGIN, state);
            }
            return be;
        }
        return null;
    }

    // ~~~~~~~~~~~~~~~~~~ Serialize ~~~~~~~~~~~~~~~~~~

    public static NbtElement serializeBlockState(BlockState state) {
        NbtElement tag = BlockState.CODEC.stable().encodeStart(NbtOps.INSTANCE, state).getOrThrow(true, (str) -> new RuntimeException(str));
        return tag;
    }

    public static BlockState deserializeBlockState(NbtElement tag) {
        return BlockState.CODEC.stable().decode(NbtOps.INSTANCE, tag).getOrThrow(true, (str) -> new RuntimeException(str)).getFirst();
    }

    @Override
    public void writeCustomDataToNbt(NbtCompound nbt) {
        super.writeCustomDataToNbt(nbt);
        nbt.put("stack", this.getStack().writeNbt(new NbtCompound()));
        if (this.hasBackpack()) {
            nbt.put("backpack", serializeBlockState(this.getDataTracker().get(HAS_BACKPACK).get()));
            nbt.put("inventory", this.inventory.toNbtList());
        }
        nbt.putBoolean("ownerIsEntity", this.ownerIsEntity);
    }

    @Override
    public void readCustomDataFromNbt(NbtCompound nbt) {
        super.readCustomDataFromNbt(nbt);
        this.setStack(ItemStack.fromNbt(nbt.getCompound("stack")));
        if (nbt.contains("backpack")) {
            this.setBackpack(deserializeBlockState(nbt.get("backpack")));
            this.inventory.readNbtList((NbtList)nbt.get("inventory"));
        }
        this.ownerIsEntity = nbt.getBoolean("ownerIsEntity");
    }

    // ~~~~~~~~~~~~~~~~~~ HOPPER SUPPORT ~~~~~~~~~~~~~~~~~~~~~

    @Override
    public void clear() {
        this.inventory.clear();
        
    }

    @Override
    public int size() {
        return this.inventory.size();
    }

    @Override
    public boolean isEmpty() {
        return this.inventory.isEmpty();
    }

    @Override
    public ItemStack getStack(int var1) {
        return this.inventory.getStack(var1);
    }

    @Override
    public ItemStack removeStack(int var1, int var2) {
        
        return this.inventory.removeStack(var1, var2);
    }

    @Override
    public ItemStack removeStack(int var1) {
        return this.inventory.removeStack(var1);
    }

    @Override
    public void setStack(int var1, ItemStack var2) {
        this.inventory.setStack(var1, var2);
    }

    @Override
    public void markDirty() {
        this.inventory.markDirty();
    }

    @Override
    public boolean canPlayerUse(PlayerEntity var1) {
        return this.inventory.canPlayerUse(var1);
    }
}
