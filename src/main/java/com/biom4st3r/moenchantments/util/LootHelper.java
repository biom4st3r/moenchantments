package com.biom4st3r.moenchantments.util;

import java.util.List;
import java.util.Random;
import java.util.function.Function;

import com.google.common.collect.ImmutableMap;

import biom4st3r.libs.biow0rks.reflection.CtorRef;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.entry.TagEntry;
import net.minecraft.loot.provider.number.BinomialLootNumberProvider;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.loot.provider.number.LootNumberProvider;
import net.minecraft.loot.provider.number.UniformLootNumberProvider;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.screen.GenericContainerScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.Vec3d;

public final class LootHelper {
    private static CtorRef<TagEntry,?> tagEntryCtor = CtorRef.getCtor(TagEntry.class, true, c->true); // First ctor
    private static CtorRef<LootContext,?> lootContextCtor = CtorRef.getCtor(LootContext.class, true, c->true); // First ctor

    public static LootNumberProvider constantRange(int i) {
        return ConstantLootNumberProvider.create(i);
    }

    public static LootNumberProvider uniformRange(int min, int max) {
        return UniformLootNumberProvider.create(min, max);
    }

    public static LootNumberProvider binomialRange(int n, int p) {
        return BinomialLootNumberProvider.create(n, p);
    }

    public static LootPoolEntry.Builder<?> tagEntryOf(TagKey<Item> items, boolean expandTag) {
        return LeafEntry.builder((weight, quality, conditions, functions) -> {
            return tagEntryCtor.newInstance(items, expandTag, weight, quality, conditions, functions);
        });
    }

    public static LootPoolEntry.Builder<?> itemEntryOf(Item item) {
        return ItemEntry.builder(item);
    }

    public static LootContext newContext(Entity entity) {
        return lootContextCtor.newInstance(new Random(), 0F, (ServerWorld)null, (Function<?,?>)null, (Function<?,?>)null, ImmutableMap.of(LootContextParameters.ORIGIN, Vec3d.ZERO, LootContextParameters.THIS_ENTITY, entity), ImmutableMap.of());
    }

    public static LootTable.Builder tableBuilder() {
        return LootTable.builder();
    }

    public static LootPool.Builder poolBuilder() {
        return LootPool.builder();
    }

    public static NamedScreenHandlerFactory getScreenWithItems(List<ItemStack> items) {
        return new NamedScreenHandlerFactory() {
            @Override
            public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
                SimpleInventory inventory = new SimpleInventory(54);
                items.forEach(inventory::addStack);
                return new GenericContainerScreenHandler(ScreenHandlerType.GENERIC_9X6, syncId, inv, inventory, 6);
            }
            @Override
            public Text getDisplayName() {
                return Text.of("");
            }
        };
    }
}
