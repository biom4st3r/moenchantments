package com.biom4st3r.moenchantments.util;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.function.Consumer;

import com.google.common.base.Preconditions;

public class OpcodeMethodVisitor extends MethodVisitor implements Opcodes {

    public static int ASM_VERSION = ASM9;
    public static int JAVA_VERSION = V16;

    public OpcodeMethodVisitor(MethodVisitor methodVisitor) {
        super(ASM_VERSION, methodVisitor);
    }

    public OpcodeMethodVisitor() {
        super(ASM_VERSION);
    }

    @Override
    public void visitVarInsn(int opcode, int var) {
        super.visitVarInsn(opcode, var);
    }

    @Override
    public void visitInsn(int opcode) {
        super.visitInsn(opcode);
    }

    /**
     * [objectref]  ->  [] 
     * @param index
     * @return
     */
    public OpcodeMethodVisitor ASTORE(int index) {
        this.visitVarInsn(ASTORE, index);
        return this;
    }

    /**
     * [value]  ->  [] 
     * @param index
     * @return
     */
    public OpcodeMethodVisitor ISTORE(int index) {
        this.visitVarInsn(ISTORE, index);
        return this;
    }

    /**
     * [value]  ->  [] 
     * @param index
     * @return
     */
    public OpcodeMethodVisitor DSTORE(int index) {
        this.visitVarInsn(DSTORE, index);
        return this;
    }

    /**
     * [value]  ->  [] 
     * @param index
     * @return
     */
    public OpcodeMethodVisitor FSTORE(int index) {
        this.visitVarInsn(FSTORE, index);
        return this;
    }

    /**
     * [value]  ->  [] 
     * @param index
     * @return
     */
    public OpcodeMethodVisitor LSTORE(int index) {
        this.visitVarInsn(LSTORE, index);
        return this;
    }

    /**
     * [arrayref, index, value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor AASTORE() {
        this.visitInsn(AASTORE);
        return this;
    }

    /**
     * [arrayref, index, value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor BASTORE() {
        this.visitInsn(BASTORE);
        return this;
    }

    /**
     * [arrayref, index, value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor CASTORE() {
        this.visitInsn(CASTORE);
        return this;
    }

    /**
     * [arrayref, index, value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor IASTORE() {
        this.visitInsn(IASTORE);
        return this;
    }

    /**
     * [arrayref, index, value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor LASTORE() {
        this.visitInsn(LASTORE);
        return this;
    }

    /**
     * [arrayref, index, value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor FASTORE() {
        this.visitInsn(FASTORE);
        return this;
    }

    /**
     * [arrayref, index, value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor DASTORE() {
        this.visitInsn(DASTORE);
        return this;
    }

    /**
     * [value]  ->  [value, value]
     * @return
     */
    public OpcodeMethodVisitor DUP() {
        this.visitInsn(DUP);
        return this;
    }

    /**
     * [value2, value1]  ->  [value2, value1, value2, value1]
     * @return
     */
    public OpcodeMethodVisitor DUP2() {
        this.visitInsn(DUP2);
        return this;
    }

    /**
     *  []  ->  objectref
     * @param index
     * @return
     */
    public OpcodeMethodVisitor ALOAD(int index) {
        this.visitVarInsn(ALOAD, index);
        return this;
    }

    /**
     *  []  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor ILOAD(int index) {
        this.visitVarInsn(ILOAD, index);
        return this;
    }

    /**
     *  []  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor DLOAD(int index) {
        this.visitVarInsn(DLOAD, index);
        return this;
    }

    /**
     *  []  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor FLOAD(int index) {
        this.visitVarInsn(FLOAD, index);
        return this;
    }

    /**
     *  []  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor LLOAD(int index) {
        this.visitVarInsn(LLOAD, index);
        return this;
    }

    /**
     * [arrayref, index]  ->  [objectref]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor AALOAD() {
        this.visitInsn(AALOAD);
        return this;
    }

    /**
     * [arrayref, index]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor BALOAD() {
        this.visitInsn(BALOAD);
        return this;
    }

    /**
     * [arrayref, index]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor CALOAD() {
        this.visitInsn(CALOAD);
        return this;
    }

    /**
     * [arrayref, index]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor IALOAD() {
        this.visitInsn(IALOAD);
        return this;
    }

    /**
     * [arrayref, index]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor LALOAD() {
        this.visitInsn(LALOAD);
        return this;
    }

    /**
     * [arrayref, index]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor FALOAD() {
        this.visitInsn(FALOAD);
        return this;
    }

    /**
     * [arrayref, index]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor DALOAD() {
        this.visitInsn(DALOAD);
        return this;
    }

    /**
     * [objectref]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor GET_FIELD(String owner, String fieldName, String desc) {
        this.visitFieldInsn(GETFIELD, owner, fieldName, desc);
        return this;
    }

    /**
     * []  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor GET_STATIC(String owner, String fieldName, String desc) {
        this.visitFieldInsn(GETSTATIC, owner, fieldName, desc);
        return this;
    }

    /**
     * [objectref]  ->  [value]
     * @param index
     * @return
     */
    public OpcodeMethodVisitor GET_FIELD(String owner, String fieldName, Class<?> desc) {
        this.GET_FIELD(owner, fieldName, org.objectweb.asm.Type.getDescriptor(desc));
        return this;
    }

    /**
     * [objectref, value]  ->  []
     * @param index
     * @return
     */
    public OpcodeMethodVisitor PUT_FIELD(String owner, String fieldName, String desc) {
        this.visitFieldInsn(PUTFIELD, owner, fieldName, desc);
        return this;
    }

    /**
     * [objectref, value]  ->  []
     * @param index
     * @return
     */
    public OpcodeMethodVisitor PUT_FIELD(String owner, String fieldName, Class<?> desc) {
        this.PUT_FIELD(owner, fieldName, org.objectweb.asm.Type.getDescriptor(desc));
        return this;
    }

    public OpcodeMethodVisitor RETURN() {
        this.visitInsn(RETURN);
        return this;
    }

    /**
     * [value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor IRETURN() {
        this.visitInsn(IRETURN);
        return this;
    }

    /**
     * [value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor DRETURN() {
        this.visitInsn(DRETURN);
        return this;
    }

    /**
     * [value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor FRETURN() {
        this.visitInsn(FRETURN);
        return this;
    }

    /**
     * [value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor LRETURN() {
        this.visitInsn(LRETURN);
        return this;
    }

    /**
     * [objectref]  ->  []
     * @return
     */
    public OpcodeMethodVisitor ARETURN() {
        this.visitInsn(ARETURN);
        return this;
    }

    /**
     * [arrayref]  ->  [length]
     * @return
     */
    public OpcodeMethodVisitor ARRAY_LENGTH() {
        this.visitInsn(ARRAYLENGTH);
        return this;
    }

    /**
     * [count]  ->  [arrayref]
     * @param arrayType
     * @return
     */
    public OpcodeMethodVisitor NEW_ARRAY(int arrayType) {
        this.visitIntInsn(NEWARRAY, arrayType);
        return this;
    }

    /**
     * [count]  ->  [arrayref]
     * @param interalName
     * @return
     */
    public OpcodeMethodVisitor ANEW_ARRAY(String interalName) {
        this.visitTypeInsn(ANEWARRAY, interalName);
        return this;
    }

    public OpcodeMethodVisitor ANEW_ARRAY(Class<?> clazz) {
        this.ANEW_ARRAY(Type.getInternalName(clazz));
        return this;
    }

    /**
     * [objectref]  ->  [objectref] 
     * @param interalName
     * @return
     */
    public OpcodeMethodVisitor CHECKCAST(String interalName) {
        this.visitTypeInsn(CHECKCAST, interalName);
        return this;
    }

    public OpcodeMethodVisitor CHECKCAST(Class<?> clazz) {
        this.visitTypeInsn(CHECKCAST, Type.getInternalName(clazz));
        return this;
    }

    /**
     * [objectref]  ->  [result] 
     * @param interalName
     * @return
     */
    public OpcodeMethodVisitor INSTANCEOF(String internalName) {
        this.visitTypeInsn(INSTANCEOF, internalName);
        return this;
    }

    /**
     * []  ->  [objectref] 
     * @param internalName
     * @return
     */
    public OpcodeMethodVisitor NEW(String internalName) {
        this.visitTypeInsn(NEW, internalName);
        return this;
    }

    public OpcodeMethodVisitor NOP() {
        this.visitInsn(NOP);
        return this;
    }

    /**
     * [value1], [value2]  ->  [result] 
     * @return
     */
    public OpcodeMethodVisitor IADD() {
        this.visitInsn(IADD);
        return this;
    }

    /**
     * [value1], [value2]  ->  [result] 
     * @return
     */
    public OpcodeMethodVisitor ISUB() {
        this.visitInsn(ISUB);
        return this;
    }

    /**
     * [value1], [value2]  ->  [result] 
     * @return
     */
    public OpcodeMethodVisitor IMUL() {
        this.visitInsn(IMUL);
        return this;
    }

    /**
     * [value1], [value2]  ->  [result] 
     * @return
     */
    public OpcodeMethodVisitor IDIV() {
        this.visitInsn(IDIV);
        return this;
    }

    /**
     * [value]  ->  []
     * @return
     */
    public OpcodeMethodVisitor POP() {
        this.visitInsn(POP);
        return this;
    }

    /**
     * [value2, value1]  ->  []<p>
     * [long]  ->  []<p>
     * [double]  ->  []
     * @return
     */
    public OpcodeMethodVisitor POP2() {
        this.visitInsn(POP);
        return this;
    }

    /**
     * [value1], [value2] → [value2], [value1] 
     * @return
     */
    public OpcodeMethodVisitor SWAP() {
        this.visitInsn(SWAP);
        return this;
    }

    /**
     * []  ->  [value]
     * @param s
     * @return
     */
    public OpcodeMethodVisitor LDC(String s) {
        this.visitLdcInsn(s);
        return this;
    }

    public OpcodeMethodVisitor LDC(Class<?> c) {
        this.visitLdcInsn(Type.getType(c));
        return this;
    }

    public OpcodeMethodVisitor LDC(Type t) {
        this.visitLdcInsn(t);
        return this;
    }

    /**
     * []  ->  [value]
     * @param s
     * @return
     */
    public OpcodeMethodVisitor LDC(Integer s) {
        this.visitLdcInsn(s);
        return this;
    }

    /**
     * []  ->  [value]
     * @param s
     * @return
     */
    public OpcodeMethodVisitor LDC(Long s) {
        this.visitLdcInsn(s);
        return this;
    }

    /**
     * []  ->  [value]
     * @param s
     * @return
     */
    public OpcodeMethodVisitor LDC(Double s) {
        this.visitLdcInsn(s);
        return this;
    }

    /**
     * []  ->  [value]
     * @param s
     * @return
     */
    public OpcodeMethodVisitor LDC(Float s) {
        this.visitLdcInsn(s);
        return this;
    }

    /**
     * [objectref, args...]  ->  [result]
     * @param internalName
     * @param name
     * @param desc
     * @return
     */
    public OpcodeMethodVisitor INVOKE_VIRTUAL(String internalName, String name, String desc) {
        this.visitMethodInsn(INVOKEVIRTUAL, internalName, name, desc, false);
        return this;
    }

    /**
     * [objectref, args...]  ->  [result]
     * @param internalName
     * @param name
     * @param desc
     * @return
     */
    public OpcodeMethodVisitor INVOKE_VIRTUAL(Method method) {
        Preconditions.checkArgument((method.getModifiers() & Modifier.STATIC) == 0);
        this.INVOKE_VIRTUAL(Type.getInternalName(method.getDeclaringClass()), method.getName(), Type.getMethodDescriptor(method));
        return this;
    }

    /**
     * [args...]  ->  [result]
     * @param internalName
     * @param name
     * @param desc
     * @return
     */
    public OpcodeMethodVisitor INVOKE_STATIC(String internalName, String name, String desc) {
        this.visitMethodInsn(INVOKESTATIC, internalName, name, desc, false);
        return this;
    }

    /**
     * [args...]  ->  [result]
     * @param internalName
     * @param name
     * @param desc
     * @return
     */
    public OpcodeMethodVisitor INVOKE_STATIC(Method method) {
        Preconditions.checkArgument((method.getModifiers() & Modifier.STATIC) != 0);
        this.INVOKE_STATIC(Type.getInternalName(method.getDeclaringClass()), method.getName(), Type.getMethodDescriptor(method));
        return this;
    }

    /**
     * [objectref, args...]  ->  [result]
     * @param internalName
     * @param name
     * @param desc
     * @return
     */
    public OpcodeMethodVisitor INVOKE_SPECIAL(String internalName, String name, String desc) {
        this.visitMethodInsn(INVOKESPECIAL, internalName, name, desc, false);
        return this;
    }

    /**
     * [objectref, args...]  ->  [result]
     * @param internalName
     * @param name
     * @param desc
     * @return
     */
    public OpcodeMethodVisitor INVOKE_INTERFACE(String internalName, String name, String desc) {
        this.visitMethodInsn(INVOKEINTERFACE, internalName, name, desc, true);
        return this;
    }

    public OpcodeMethodVisitor INVOKE_INTERFACE(Method method) {
        Preconditions.checkArgument((method.getModifiers() & Modifier.STATIC) != 0);
        this.INVOKE_INTERFACE(Type.getInternalName(method.getDeclaringClass()), method.getName(), Type.getMethodDescriptor(method));
        return this;
    }

    public OpcodeMethodVisitor TABLE_SWITCH(int min, int max, Label dflt, Label[] labels) {
        this.visitTableSwitchInsn(min, max, dflt, labels);
        return this;
    }
    
    public OpcodeMethodVisitor LABEL(Label label) {
        this.visitLabel(label);
        return this;
    }

    // ~~~~~~~~Statics~~~~~~~~

    public static Label[] getLabels(int i) {
        Label[] ls = new Label[i];
        for (int j = 0; j < ls.length; j++) {
            ls[i] = new Label();
        }
        return ls;
    }

    // ~~~~~~~~Custom~~~~~~~~

    public OpcodeMethodVisitor START() {
        this.visitCode();
        return this;
    }

    public void FINISH(int maxStack, int maxLocals) {
        this.visitMaxs(maxStack, maxLocals);
        this.visitEnd();
    }

    /**
     * []  ->  [objectref]
     * @param internalName
     * @param desc
     * @param loadArgs
     */
    public OpcodeMethodVisitor _CTOR(String internalName, String desc, Consumer<OpcodeMethodVisitor> loadArgs) {
        this.NEW(internalName);
        this.DUP();
        loadArgs.accept(this);
        this.INVOKE_SPECIAL(internalName, "<init>", desc);
        return this;
    }

    public OpcodeMethodVisitor _CTOR(Class<?> clazz, String desc, Consumer<OpcodeMethodVisitor> loadArgs) {
        this.NEW(Type.getInternalName(clazz));
        this.DUP();
        loadArgs.accept(this);
        this.INVOKE_SPECIAL(Type.getInternalName(clazz), "<init>", desc);
        return this;
    }

    /**
     * []  ->  [value]
     * @param n
     * @return
     */
    public OpcodeMethodVisitor PUT_NUM(Number n) {
        if (n instanceof Float f) {
            this._LOAD_F(f);
        }
        else if (n instanceof Integer) {
            this._LOAD_I((Integer)n);
        }
        else if (n instanceof Double) {
            this._LOAD_D((Double)n);
        }
        else if (n instanceof Long) {
            this._LOAD_J((Long)n);
        } else {
            throw new RuntimeException("_LOAD_NUM Failed? %s");
        }
        return this;
    }

    public OpcodeMethodVisitor PUT_OBJ(Object o) {
        if(o instanceof Number) PUT_NUM((Number) o);
        else if(o instanceof String) this.LDC((String)o);
        else if(o == null) this.visitInsn(ACONST_NULL);
        else throw new IllegalArgumentException();
        return this;
    }

    /**
     * []  ->  [value]
     * @param val
     * @return
     */
    public OpcodeMethodVisitor _LOAD_J(long val) {
        if (val == 0L) {
            this.visitInsn (LCONST_0);
        } else if (val == 1L) {
            this.visitInsn (LCONST_1);
        } else {
            this.visitLdcInsn(val);
        }
        return this;
    }

    /**
     * []  ->  [value]
     * @param val
     * @return
     */
    public OpcodeMethodVisitor _LOAD_D(double val) {
        if (val == -1) {
            this.visitInsn(DCONST_0);
        }
        else if (val == 0) {
            this.visitInsn(DCONST_1);
        }
        else {
            this.LDC(val);
        }
        return this;
    }

    /**
     * []  ->  [value]
     * @param val
     * @return
     */
    public OpcodeMethodVisitor _LOAD_F(float val) {
        if (val == -1) {
            this.visitInsn(FCONST_0);
        }
        else if (val == 0) {
            this.visitInsn(FCONST_1);
        }
        else if (val == 0) {
            this.visitInsn(FCONST_2);
        }
        else {
            this.LDC(val);
        }
        return this;
    }

    /**
     * []  ->  [value]
     * @param val
     * @return
     */
    public OpcodeMethodVisitor _LOAD_I(int val) {
        if (val == -1) {
            this.visitInsn(ICONST_M1);
        }
        else if (val == 0) {
            this.visitInsn(ICONST_0);
        }
        else if (val == 1) {
            this.visitInsn(ICONST_1);
        }
        else if (val == 2) {
            this.visitInsn(ICONST_2);
        }
        else if (val == 3) {
            this.visitInsn(ICONST_3);
        }
        else if (val == 4) {
            this.visitInsn(ICONST_4);
        }
        else if (val == 5) {
            this.visitInsn(ICONST_5);
        }
        else if (val <= Byte.MAX_VALUE && val >= Byte.MIN_VALUE) {
            this.visitIntInsn(BIPUSH, val);
        }
        else if (val <= Short.MAX_VALUE && val >= Short.MIN_VALUE) {
            this.visitIntInsn(SIPUSH, val);
        } else {
            this.visitLdcInsn(val);
        }
        return this;
    }

    public static OpcodeClassVisitor newCv(ClassVisitor cv) {
        return new OpcodeClassVisitor(cv);
    }
}
